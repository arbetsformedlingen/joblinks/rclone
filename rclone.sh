#!/usr/bin/env bash
set -eEu -o pipefail
#set -x
rclone_args="--retries-sleep 1m --retries 10" #-v -v -v -v

mode=""
src=""
trg=""
secrets=""
flock=0
direction=download
while [ "$#" -gt 0 ]; do
    case "$1" in
        --op)    mode="$2"; shift ;;
        --secrets) secrets="$2"; shift ;;
        --src)   src="$2"; shift ;;
        --trg)   trg="$2"; shift ;;
        --flock) flock=1;;
        --)      shift; break;;
        *)       echo "**** unknown opt: $1" >&2; exit 1;;
    esac
    shift
done


# Required params from here
test -f "${secrets}" || { echo "**** missing config ${secrets}">&2; exit 1; }

if [ -z "$mode" ] || [ -z "$src" ] || [ -z "$trg" ]; then
    echo "**** missing required parameters --op, --src and --trg">&2; exit 1
fi


# Check if we do upload
if [[ $trg =~ : ]]; then direction=upload; fi

# Only flock upload with a single file
if [ "$flock" = 1 ] && [ "$direction" = upload ] && [ ! -f "$src" ]; then
    echo "**** uploading with flock requires an existing source file (and will not work with directory): $src">&2; exit 1
fi

# --s3-no-check-bucket needed to make it work with minio, for some reason
rclone_args="$rclone_args --s3-no-check-bucket --config $secrets $@"


function handle_flock() {
    local direction="$1"
    local src="$2"
    local trg="$3"
    local secrets="$4"

    if [ "$direction" = download ]; then
        if [ `rclone $rclone_args ls "$src" | wc -l` == 0 ] || ! rclone $rclone_args copyto "$src".md5 "$trg".md5.lock || [ ! -f "$trg".md5.lock ] ; then
            echo "rclone: $src.md5 not found, creating new" >&2
            echo init > "$trg".md5.lock
            rclone $rclone_args copyto "$trg".md5.lock "$src".md5 || { echo "**** cannot create init lock $src.md5">&2; return 1; }
        fi
        if [ ! -s "$trg".md5.lock ]; then echo "**** copy $src.md5 $trg.md5.lock gave empty file" >&2; return 1; fi
    else
        rclone $rclone_args copyto "$trg".md5 "$src".md5.lock.finalcheck || { echo "**** cannot find remote $trg.md5" >&2; return 1; }

        if [ ! -s "$src".md5.lock.finalcheck ]; then echo "**** copy $trg.md5 $src.md5.lock.finalcheck gave empty file" >&2; return 1; fi
        if [ ! "$(cat $src.md5.lock)" = "$(cat $src.md5.lock.finalcheck)" ]; then
            echo "**** remote has changed, will not overwrite flocked file">&2; return 1
        fi
        md5sum < "$src" | cut -f 1 -d' ' > "$src".md5
        rclone $rclone_args copyto "$src".md5 "$trg".md5
    fi
}



if [ -n "$src" ] && [ -n "$trg" ] && [ -n "$mode" ]; then

    if [ "$flock" = 1 ]; then
        handle_flock "$direction" "$src" "$trg" "$secrets" || { echo "**** flock failed" >&2; exit 1; }
    fi

    rclone_args="$rclone_args $@"

    if [ "$mode" = copy ] && [ "$direction" = download ] && [ -f "$trg" ]; then
        echo "info: rclone: moving target file before copying single file, to comply to rclone rules (mv --backup=numbered "$trg" "$trg".backup)" >&2
        mv --backup=numbered "$trg" "$trg".backup
    fi

    case "$mode" in
        "copy")     rclone $rclone_args copyto $src $trg;;
        "sync")     rclone $rclone_args sync   $src $trg;;
        *) echo "**** bad arg $mode" >&1; exit 1 ;;
    esac
fi
