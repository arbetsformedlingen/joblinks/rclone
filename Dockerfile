FROM docker.io/library/ubuntu:22.04 AS base

COPY rclone.sh /usr/local/bin/

RUN apt-get -y update &&\
        apt-get install -yq --no-install-recommends --fix-missing unzip ca-certificates wget &&\
        wget 'https://downloads.rclone.org/v1.67.0/rclone-v1.67.0-linux-amd64.zip' &&\
        unzip rclone-v1.67.0-linux-amd64.zip &&\
        mv rclone-*/rclone /usr/local/bin &&\
        apt-get clean && apt-get -y autoremove &&\
        rm -rf /var/lib/apt/lists/* rclone-*


###############################################################################
FROM base AS test
RUN  rclone --version \
     && touch /.tests-successful


###############################################################################
FROM base

COPY --from=test /.tests-successful /

#need writable home for the "mc" steps in the rclone wrapper
WORKDIR /tmp
ENV HOME /tmp
ENTRYPOINT [ "bash", "/usr/local/bin/rclone.sh" ]
